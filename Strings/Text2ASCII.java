package Strings;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Text2ASCII {

	public static void main(String[] args) throws IOException {
		System.out.println("Bitte Text eingeben:");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String input = br.readLine();
		for(int i=0; i < input.length(); i++) {
			System.out.printf("%03d", (int)input.charAt(i));
			System.out.print(" ");
		}
	}

}
