package Strings;

import java.util.Scanner;

public class Password {

	static public boolean isSafe10Char(String pw) {
		final int MIN_LEN = 10;
		if (pw.length() >= MIN_LEN) {
			return true;
		} else {
			System.out.println("Das Passwort ist " + (MIN_LEN - pw.length()) + " Zeichen zu kurz.");
			return false;
		}
	}

	static public boolean isSafeUpperDigit(String pw) {
		boolean isDigit = false, isUpper = false;
		for (int i = 0; i < pw.length(); i++) {
			if (Character.isUpperCase(pw.charAt(i))) {
				isUpper = true;
			}
			if (Character.isDigit(pw.charAt(i))) {
				isDigit = true;
			}
		}

		if (isUpper && isDigit) {
			return true;
		} else {
			System.out.println("Das Passwort muss Großbuchstaben und Ziffern enthalten.");
			return false;
		}

	}

	static public boolean isSafeLowerSpecial(String pw) {
		boolean isSpecial = false, isLower = false;
		for (int i = 0; i < pw.length(); i++) {
			char c = pw.charAt(i);
			if (Character.isLowerCase(c)) {
				isLower = true;
			}
			if (!Character.isLetterOrDigit(c)) {
				isSpecial = true;
			}
		}

		if (isLower && isSpecial) {
			return true;
		} else {
			System.out.println("Das Passwort muss Kleinbuchstaben und Sonderzeichen enthalten.");
			return false;
		}

	}

	public static void main(String[] args) {
		System.out.println("Bitte Passwort eingeben: ");
		Scanner input = new Scanner(System.in);

		String pw = input.nextLine();

		if (Password.isSafe10Char(pw) && Password.isSafeLowerSpecial(pw) && Password.isSafeUpperDigit(pw)) {
			System.out.println("Das Passwort ist sicher.");
		} else {
			System.out.println("Das Passwort ist nicht sicher.");
		}
		input.close();

	}

}
