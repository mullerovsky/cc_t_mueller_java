package Strings;

public class FirstLastName {
	private String firstName = "";
	private String lastName = "";
	private String name = "";
	private String initials = "";

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
		setInitials();
		setName();
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
		setInitials();
		setName();
	}

	public String getInitials() {
		return initials;
	}

	private void setInitials() {
		initials = "";
		if (getFirstName().length() > 0)
			initials += getFirstName().substring(0, 1);
		if (getLastName().length() > 0)
			initials += getLastName().substring(0, 1);
	}

	public String getName() {
		return name;
	}

	private void setName() {
		name = getFirstName() + " " + getLastName();
	}

	public FirstLastName(String firstName, String lastName) {
		setFirstName(firstName);
		setLastName(lastName);
	}

	public int getFirstNameLength() {
		return firstName.length();
	}

	public int getLastNameLength() {
		return lastName.length();
	}

	public int getNameLength() {
		return name.length();
	}

	public static void main(String[] args) {
		FirstLastName myName = new FirstLastName("Thomas", "Müller");

		System.out.println(myName.getFirstName() + "\tLänge: " + myName.getFirstNameLength());
		System.out.println(myName.getLastName() + "\tLänge: " + myName.getLastNameLength());
		System.out.println(myName.getInitials());
		System.out.println(myName.getName() + "\tLänge: " + myName.getNameLength());
		System.out.println();

		myName.setLastName("Schmidt");

		System.out.println(myName.getFirstName() + "\tLänge: " + myName.getFirstNameLength());
		System.out.println(myName.getLastName() + "\tLänge: " + myName.getLastNameLength());
		System.out.println(myName.getInitials());
		System.out.println(myName.getName() + "\tLänge: " + myName.getNameLength());
		System.out.println();

	}

}
