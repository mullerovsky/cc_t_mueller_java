# LIESMICH #

### Sehen die Umlaute selsam aus? ###

* **Window -> Preferences -> General -> Workspace**
* Im rechten Bereich erschein unten links ein Feld ** Text file encoding**.
* Den Haken bei **Other** setzen und **UTF-8** aus dem Dropdown-Menü wählen.

### Wofür ist dieses repository gedacht? ###

* Diese Repository ist für die Teilnehmer des Kurses **Java Entwickler mit Oracle Certified Associate SE** angeboten durch die [ComCave]( https://www.comcave.de/ ) gedacht.
* Hier kann mein Java Quellcode oder andere relevanten Dokumente eingesehen werden.

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Wie richte ich dieses Repository in Eclipse ein? ###

* **Window -> Show View -> Other** aufrufen.
* Hier suchen nach **Git -> Git Repositories**.
* Im linken unteren Fenster der Eclipse IDE erscheint **Git Repositories**.
* Beim Rechtsklick in diesem Bereich erschein ein Kontextmenü, **Clone a Git Repository** auswählen.
* In das Feld **URI** die Adressedes Repositorys eintragen ( **https://bitbucket.org/mullerovsky/cc_t_mueller_java** ).
* **Next** und noch einmal **Next** klicken.
* Den Eintrag für das **Directory** zu **C:\Java\cc_t_mueller_java** oder zu einem anderen gewünschten Wert ändern.
* **Finish** und fertig! (... beinahe fertig).
* Im linken unteren Bereich von Eclipse erscheint das **cc_t_mueller_java** Repository.
* Rechtsklick auf das Repository und **Import Projects** aus dem Kontextmenu auswählen.
* Das Projekt erscheint nun im Package Explorer (linker oberer Bereich von Eclipse). **Fertig!** :-)

### Richtlinien zum Mitwirken ###

* Dieses Repo ist in erster Linie zur Einsicht in den Quellcode meiner Java Übungen gedacht und weniger zur Mitarbeit. 
* Aber unter Umständen nehme ich auch Änderungen an den Quellen auf. 
* Über Rückmeldungen freue ich mich selbstverständlich.

### Wen kann ich ansprechen? ###

* Thomas Müller
