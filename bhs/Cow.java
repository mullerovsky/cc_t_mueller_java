package bhs;

public class Cow {
	private String name;		//name of cow
	private String colorMain;	//main color
	private String colorSpots;	//color of spots
	private double weight;		//weight in kg
	private double amountMilk;	//amount of milk in liter


	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColorMain() {
		return this.colorMain;
	}

	public void setColorMain(String colorMain) {
		this.colorMain = colorMain;
	}

	public String getColorSpots() {
		return this.colorSpots;
	}

	public void setColorSpots(String colorSpots) {
		this.colorSpots = colorSpots;
	}

	public double getWeight() {
		return this.weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getAmountMilk() {
		return this.amountMilk;
	}

	public void setAmountMilk(double amountMilk) {
		this.amountMilk = amountMilk;
	}

	{
		this.set
	}
	
	public Cow() {
		this("Elsa");
	}
	
	public Cow(String name) {
		this(name, "weiß", "braun");
	}
	
	
	
	public Cow(String name, String colorMain, String colorSpots, double weight, double amountMilk) {
		setName(name);
		setColorMain(colorMain);
		setColorSpots(colorSpots);
		setWeight(weight);
		setAmountMilk(amountMilk);
	}


	public void walk(double distance /* distance walked in km */) {
		// the weight decreases with the distance walked. (One kg per one km walked).
		double newWeight = getWeight() - distance;
		if(newWeight > 100) {
			setWeight(newWeight);
		}
		else {
			System.out.println("Die Kuh kann nicht so weit laufen. Sie ist dafür zu leicht.");
		}
	}


	public void eat(double amount /* amount of food eaten */) {
		// the cow puts on 1kg of weight per 10kg of food eaten and the amount of milk increases by 1 liter
		double newWeight = getWeight() + 0.1*amount;
		double newAmountMilk = getAmountMilk() + 0.1*amount;
		setWeight(newWeight);
		setAmountMilk(newAmountMilk);
	}
	
	public void milk(double time /* time the cow is milked in minutes */) {
		// the cow gives 1 liter of milk per minute
		double newAmountMilk = getAmountMilk() - time;
		if(newAmountMilk > 0) {
			setAmountMilk(newAmountMilk);
		}
		else {
			System.out.println("Die Kuh kann nicht mehr als ihr Milchmenge an Milch geben. ");
		}
	}

	@Override
	public String toString() {
		String cowText = "";
		String.format("Hie ein paar Informatonen zur Kuh %s:\n", getName());
		System.out.printf("Ihr Fell ist %s und die Farbe der Flecken ist %s.\n", getColorMain(), getColorSpots());
		System.out.printf("Sie wiegt %.0f kg und hat eine Milchmenge von %.0f Litern.", getWeight(), getAmountMilk());
		return cowText;
	}

	public static void main(String[] args) {
		Cow cow01 = new Cow("Elsa", "weiß", "braun", 850, 30);
		
		cow01.toString();
		
		cow01.walk(20);
		
		cow01.toString();
		
		cow01.eat(20);
		
		cow01.toString();
		
		
	}

}
