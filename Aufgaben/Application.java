package Aufgaben;

public class Application {

	public static void main(String[] args) {
		Address manufacturer = new Address("Werk", "Köln-Niehl", "Henry-Ford-Straße", "1", 50735, "Köln");
		Address peter = new Address("Peter", "Musterautohaus", "Langer Weg", "12A", 1239, "Dresden");
		Address eva = new Address("Eva", "Musterautohaus", "Langer Weg", "12A", 1239, "Dresden");
		Address luca = new Address("Luca", "Musterautohaus", "Langer Weg", "12A", 1239, "Dresden");

		
		System.out.println();
		MotorVehicle defaultCar = new MotorVehicle();
		defaultCar.printMotorVehicle();
		
		System.out.println();
		MotorVehicle car1 = new MotorVehicle(new CarSize(3, 1.5, 1.5), 1300, "JEEP", manufacturer, eva, false);
		car1.printMotorVehicle();

		System.out.println();
		MotorVehicle car2 = new MotorVehicle(new CarSize(3.5, 1.3, 1.3), 1100, "SPORTSCAR", manufacturer, luca, false);
		car2.printMotorVehicle();

		System.out.println();
		MotorVehicle car3 = new MotorVehicle(new CarSize(4.53, 1.78, 1.5), 1400, "JEEP", manufacturer, peter, false);
		car3.printMotorVehicle();

	}

}
