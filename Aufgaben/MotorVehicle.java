package Aufgaben;

public class MotorVehicle {
	private CarSize myCarSize;
	private double weight;
	private String carMake;
	private Address start, destination;
	private boolean privateClient;

	public CarSize getMyCarSize() {
		return this.myCarSize;
	}

	public void setMyCarSize(CarSize myCarSize) {
		this.myCarSize = myCarSize;
	}

	public double getWeight() {
		return this.weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public String getCarMake() {
		return this.carMake;
	}

	public void setCarMake(String carMake) {
		this.carMake = carMake;
	}

	public Address getStart() {
		return this.start;
	}

	public void setStart(Address start) {
		this.start = start;
	}

	public Address getDestination() {
		return this.destination;
	}

	public void setDestination(Address destination) {
		this.destination = destination;
	}

	public boolean isPrivateClient() {
		return this.privateClient;
	}

	public void setPrivateClient(boolean privateClient) {
		this.privateClient = privateClient;
	}

	{
		this.myCarSize = new CarSize(3.4, 1.68, 1.86);
		this.weight = 540;
		this.carMake = "Ford Model T";
		this.privateClient = false;
		this.start = new Address("Werk", "Köln-Niehl", "Henry-Ford-Straße", "1", 50735, "Köln");
		this.destination = new Address("Musterautohaus", "Wareneingang", "Langer Weg", "12A", 1239, "Dresden");
	}
	
	public MotorVehicle() {
	}

	public MotorVehicle(CarSize myCarSize, double weight, String carMake, Address start,
			Address destination, boolean privateClient) {
		this.myCarSize = myCarSize;
		this.weight = weight;
		this.carMake = carMake;
		this.start = start;
		this.destination = destination;
		this.privateClient = privateClient;
	}

	public void printMotorVehicle() {
		System.out.printf("Größe des Fahrzeugs: "); 
		getMyCarSize().printCarSize();
		System.out.println();
		System.out.printf("Gewicht: %.2f kg\n", getWeight());
		System.out.println("Automarke: " + getCarMake());
		System.out.print("Absenderadresse: "); 
		getStart().printAddress();
		System.out.print("Zieladresse: "); 
		getDestination().printAddress();
		if(!isPrivateClient()) System.out.println("Kein Selbstabholer\n");
	}

}