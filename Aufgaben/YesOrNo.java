package Aufgaben;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class YesOrNo {

	public static void main(String[] args) throws IOException {
		
		System.out.println("Antworten sie mit Ja (j oder J) oder Nein (n oder N):");

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));		
		String answer = br.readLine();
		
		if (answer.equals("j") || answer.equals("J")) {
			System.out.println("Sie haben mit \"ja\" geantwortet.");
		}
		else if (answer.equals("n") || answer.equals("N")) {
			System.out.println("Sie haben mit \"nein\" geantwortet.");
		}
		else {
			System.out.println("Sie haben eine falsche Eingabe getätigt.");
		}
	}

}
