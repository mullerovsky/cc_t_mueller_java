package Aufgaben;

public class TableOfTruth {
	//static final boolean[] TRUTH_VALUES = new boolean[] {false, true}; 
	static final boolean[] TRUTH_VALUES = {false, true}; 

	
	public static void main(String[] args) {
		
		System.out.println("Logische Operatoren:");
		System.out.println("====================");
		
		for (boolean a : TRUTH_VALUES) {
			for (boolean b : TRUTH_VALUES) {
				System.out.println(a + " && " + b +" liefert " + (a&&b));
				System.out.println(a + " || " + b +" liefert " + (a||b));
				System.out.println(a + "  ^ " + b +" liefert " + (a^b));
				System.out.println();
			}
		}	
	}

}
