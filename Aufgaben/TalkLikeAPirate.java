package Aufgaben;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TalkLikeAPirate {
	static final private String VOWELS = "AEIOUaeiou";

	public static void main(String[] args) throws IOException {
		System.out.println("Bitte Text eingeben (Zum Beenden \"Ende\" eintippen):");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		String input = "";
		while (!input.equalsIgnoreCase("Ende")) {

			// String pirateVowel = "";
			for (int i = 0; i < input.length(); i++) {
				char c = input.charAt(i);

				if (VOWELS.contains(String.valueOf(c))) {
					if (i < input.length() - 1) {
						if (VOWELS.contains(String.valueOf(input.charAt(i + 1)))) {
							System.out.print(c);
						} else {
							System.out.print(c + "rr");
						}
					} else {
						System.out.print(c + "rr");
					}
				} else {
					System.out.print(c);
				}
			}
			System.out.println();

			input = br.readLine();
		}

	}
}
