package Aufgaben;

public class WitchSwitch {
	final static private String MORNING_STRING = "Morgens früh um ";
	final static private String[] WITCH_TEXT = { 
		"kommt die kleine Hex.", 
		"schabt sie gelbe Rüben.", 
		"wird Kaffee gemacht.",
		"geht sie in die Scheun'.", 
		"hackt sie Holz und Spän'.", 
		"Feuert an um 11,", 
		"kocht dann bis um 12:",
		"Fröschebein und Krebs und Fisch.",
		"Hurtig Kinder kommt zu Tisch!" 
	};

	public static void main(String[] args) {
		for (int clock = 6; clock <= 12; clock++) {
			String witchLine;
			switch (clock) {
				case 11:
					witchLine = WITCH_TEXT[clock - 6];
					break;
				case 12:
					witchLine = WITCH_TEXT[clock - 6] + "\n" + WITCH_TEXT[clock - 5] + "\n" + WITCH_TEXT[clock - 4];
					break;
				default:
					System.out.println(MORNING_STRING + clock);					
					witchLine = WITCH_TEXT[clock - 6];
					break;
			}
			System.out.println(witchLine);
		}

	}

}
