package Aufgaben;

public class Address {
	private String firstName, lastName, street, houseNumber, town;
	private int postcode;

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getStreet() {
		return this.street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getHouseNumber() {
		return this.houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	public String getTown() {
		return this.town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public int getPostcode() {
		return this.postcode;
	}

	public void setPostcode(int postcode) {
		this.postcode = postcode;
	}

	public Address() {
	}

	public Address(String firstName, String lastName, String street, String houseNumber, int postcode, String town) {
		setFirstName(firstName);
		setLastName(lastName);
		setStreet(street);
		setHouseNumber(houseNumber);
		setTown(town);
		setPostcode(postcode);
	}

	public void printAddress() {
		System.out.printf("%s %s %s %s %05d %s\n", 
				getFirstName(), getLastName(), getStreet(), getHouseNumber(), getPostcode(), getTown());
	}
}
