package Aufgaben;

public class ASCIIChart {

	public static void main(String[] args) {
		System.out.println("Dezimal\tHex\tZeichen\t\tDezimal\tHex\tZeichen");
		System.out.println("---------------------------\t-----------------------");

		String charString;

		final int START = 32; // first ASCII
		final int STOP = 127; // last ASCII 
		final int OFFSET = (STOP + 1 - START)/2;
		
		for (int i = START; i < START+OFFSET; i++) {
			for (int j = 0; j <= 1; j++) { // we iterate through left and right side of the chart
				int k = i + j * OFFSET; // k is the ASCII value, every second time there will be an offset.
				
				if (k == 32) {
					charString = "\tLeerzeichen\t";
				} else if (k == 127) {
					charString = "\tLöschen\t\t";
				} else {
					charString = "\t" + (char) k + "\t\t";
				}

				System.out.print(k + "\t#" + Integer.toHexString(k) + charString);
			}
			System.out.println();

		}
	}
}
