package Aufgaben;

import java.util.Scanner;

public class BMI {

	private int age;
	private double mass, height;

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public double getMass() {
		return mass;
	}

	public void setMass(double mass) {
		this.mass = mass;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}
	
	public BMI(int age, double mass, double height) {
		super();
		this.age = age;
		this.mass = mass;
		this.height = height;
	}

	public int returnBMI() {
		return (int)(mass/(height*height));
	}
	
	public String generateFriendlyMessage() {
		int bmi = this.returnBMI();
		String message, messageTooYoung, messageLow, messageRight, messageHigh;
		
		message = "Dein BMI beträgt " + bmi + ".\n"; 
		messageTooYoung = "In deinem Alter ist der BMI noch nicht sehr aussagekräftig.\n";
		messageLow = "Für deine Altersgruppe ist dieser Wert eher niedrig.\n";
		messageRight = "Für deine Altersgruppe ist dieser Wert genau richtig.\n";
		messageHigh = "Für deine Altersgruppe ist dieser Wert möglicherweise zu hoch.\n";
		
		if ( age < 19 ) {
			message = message + messageTooYoung;
		} 
		else if ( age < 25 ) {
			if (bmi < 19) {
				message = message + messageLow;
			}
			else if (bmi <= 24) {
				message = message + messageRight;
			}
			else {
				message = message + messageHigh;
			}
		}
		else if ( age < 35 ) {
			if (bmi < 20) {
				message = message + messageLow;
			}
			else if (bmi <= 25) {
				message = message + messageRight;
			}
			else {
				message = message + messageHigh;
			}			
		}
		else if ( age < 45 ) {
			if (bmi < 21) {
				message = message + messageLow;
			}
			else if (bmi <= 26) {
				message = message + messageRight;
			}
			else {
				message = message + messageHigh;
			}			
		}
		else if ( age < 55 ) {
			if (bmi < 22) {
				message = message + messageLow;
			}
			else if (bmi <= 27) {
				message = message + messageRight;
			}
			else {
				message = message + messageHigh;
			}			
		}
		else if ( age < 65 ) {
			if (bmi < 23) {
				message = message + messageLow;
			}
			else if (bmi <= 28) {
				message = message + messageRight;
			}
			else {
				message = message + messageHigh;
			}			
		}
		else {
			if (bmi < 24) {
				message = message + messageLow;
			}
			else if (bmi <= 29) {
				message = message + messageRight;
			}
			else {
				message = message + messageHigh;
			}			
		}
		
		return message;
	}
	
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		System.out.println("Bitte Alter in Jahren eingeben:");
		int age = input.nextInt();
		System.out.println("Bitte Masse in kg eingeben:");
		double mass = input.nextDouble();
		System.out.println("Bitte Größe in Meter eingeben:");
		double height = input.nextDouble();
		input.close();
		
		BMI myBMI = new BMI(age, mass, height);
		
		System.out.println();
		System.out.println(myBMI.generateFriendlyMessage());
	}
}
