package Aufgaben;

import java.util.Locale;
import java.util.Scanner;
import java.lang.Math;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class Gesamtpreis {
	private int quantity;
	private double price;

	public int getQuantity() {
		return this.quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getPrice() {
		return this.price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Gesamtpreis(int quantity, double price) {
		setQuantity(quantity);
		setPrice(price);
	}

	public double returnTotalPrice() {
		double total;

		total = quantity * price;

		if (this.quantity > 50) {
			total = total - 0.1 * total; // 10% discount
		} else if (this.quantity > 10) {
			total = total - 0.5 * total; // 5% discount
		}
		// total = Math.round((total)*100/100);
		total = Math.floor(total * 100) / 100;

		return total;
	}

	public static void main(String[] args) {
		Locale locale = new Locale("de", "DE");
		System.out.println("Bitte den Stückreis in Euro eingeben:");
		Scanner input = new Scanner(System.in);
		double price = input.nextDouble();
		System.out.println("Bitte die Anzahl eingeben:");
		int quantity = input.nextInt();
		input.close();

		Gesamtpreis myTotal = new Gesamtpreis(quantity, price);

		// Locale locale = new Locale("de", "DE");
		DecimalFormat df2 = (DecimalFormat) NumberFormat.getNumberInstance(locale);
		df2.applyPattern("#.##");

		System.out.println("Der Gesamtpreis beträgt: " + df2.format(myTotal.returnTotalPrice()) + " Euro.\n");
	}

}
