package Aufgaben;

import java.util.Scanner;

public class BMI2 {

	private int age;
	private double mass, height;

	static final int[] LOW_BMI = { 0, 19, 20, 21, 22, 23, 24 };
	static final int[] HIGH_BMI = { 0, 24, 25, 26, 27, 28, 29 };	

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public double getMass() {
		return mass;
	}

	public void setMass(double mass) {
		this.mass = mass;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public BMI2(int age, double mass, double height) {
		this.age = age;
		this.mass = mass;
		this.height = height;
	}

	private int ageToIndex(int age) {
		int i; 
		if ( age < 19) {
			i = 0;
		}
		else if ( age < 25) {
			i = 1;
		}
		else if ( age < 35) {
			i = 2;
		}
		else if ( age < 45) {
			i = 3;
		}
		else if ( age < 55) {
			i = 4;
		}
		else if ( age < 65) {
			i = 5;
		}
		else {
			i = 6;
		}
		return i;
	}
	
	public int returnBMI() {
		return (int)(mass/(height*height));
	}
	
	public String generateFriendlyMessage() {
		int bmi = this.returnBMI();
		String message, messageTooYoung, messageLow, messageRight, messageHigh;
		
		message = "Dein BMI beträgt " + bmi + ".\n"; 
		messageTooYoung = "In deinem Alter ist der BMI noch nicht sehr aussagekräftig.\n";
		messageLow = "Für deine Altersgruppe ist dieser Wert eher niedrig.\n";
		messageRight = "Für deine Altersgruppe ist dieser Wert genau richtig.\n";
		messageHigh = "Für deine Altersgruppe ist dieser Wert möglicherweise zu hoch.\n";
		
		if (0 == LOW_BMI[ageToIndex(age)]) {
			message = message + messageTooYoung;
		}
		else if (bmi < LOW_BMI[ageToIndex(age)]) {
			message = message + messageLow;
		}
		else if (bmi <= HIGH_BMI[ageToIndex(age)]) {
			message = message + messageRight;
		}
		else {
			message = message + messageHigh;
		}		
		
		return message;
	}
	
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		System.out.println("Bitte Alter in Jahren eingeben:");
		int age = input.nextInt();
		System.out.println("Bitte Masse in kg eingeben:");
		double mass = input.nextDouble();
		System.out.println("Bitte Größe in Meter eingeben:");
		double height = input.nextDouble();
		input.close();
		
		BMI2 myBMI = new BMI2(age, mass, height);
		
		System.out.println();
		System.out.println(myBMI.generateFriendlyMessage());
	}
}
