package Aufgaben;

public class CarSize {
	private double length, width, height;

	public double getLength() {
		return this.length;
	}

	public void setLength(double length) {
		this.length = length;
	}

	public double getWidth() {
		return this.width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return this.height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public CarSize() {
	}



	public CarSize(double length, double width, double height) {
		setLength(length);
		setWidth(width);
		setHeight(height);
	}

	public void printCarSize() {
		System.out.printf("%.2f x %.2f x %.2f", getLength(), getWidth(), getHeight());
	}
	
	public boolean equals(CarSize other) {
		if (this == other)
			return true;
		if (Double.doubleToLongBits(height) != Double.doubleToLongBits(other.height))
			return false;
		if (Double.doubleToLongBits(length) != Double.doubleToLongBits(other.length))
			return false;
		if (Double.doubleToLongBits(width) != Double.doubleToLongBits(other.width))
			return false;
		return true;
	}

}
