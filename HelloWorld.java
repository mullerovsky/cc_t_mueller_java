public class HelloWorld {

	public static void main(String[] args) {
		System.out.println("Welcome to the World of Java!");
		
		String s1 = "Hallo";
		String s2 = "Hallo";
		boolean b = s1 == "Hal" + "lo";
		
		System.out.println(s2 == "Hal" + "lo");
		System.out.println("Hallo" == "Hal" + "lo");
		System.out.println(b);
		
	}

}
