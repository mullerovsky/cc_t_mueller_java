package homework.arrays;

import java.lang.Math;

public class Lotto {
	final static int ALL_NUM = 49;
	final static int DRAW_NUM = 6;
	private int[] myNumbers = new int[DRAW_NUM];

	public int[] getMyNumbers() {
		return this.myNumbers;
	}

	public void setMyNumbers(int[] myNumbers) {
		this.myNumbers = myNumbers;
	}

	public Lotto() {
	}

	public Lotto(int[] myNumbers) {
		setMyNumbers(myNumbers);
	}

	private static int[] randperm(int n) {
		double[] d = new double[n]; // array of random doubles
		for (int i = 0; i < n; i++) {
			d[i] = Math.random();
		}
		
		int[] index = new int[n]; // index of those doubles
		for (int i = 0; i < n; i++) {
			index[i] = i + 1;
		}
		
		// bubblesort, sorting the random doubles by size 
		// when all the random doubles are sorted, there indices end up in random order
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n - i - 1; j++) {
				if (d[j] > d[j + 1]) {
					double tmpD = d[j];
					d[j] = d[j + 1];
					d[j + 1] = tmpD;
					
					// when we swap a random double, we swap there indices too.
					int tmpI = index[j];
					index[j] = index[j + 1];
					index[j + 1] = tmpI;
				}

			}

		}
		return index;
	}

	public void drawNumbers() {
		int[] randInt = randperm(ALL_NUM); // we shuffle all 49 numbers
		int[] tmpNumbers = new int[DRAW_NUM];
		for (int i = 0; i < DRAW_NUM; i++) { // now we select the first 6 of them
			tmpNumbers[i] = randInt[i];
		}
		setMyNumbers(tmpNumbers);
	}

	public void printNumbers() {
		System.out.print("Die Glückszahlen lauten:");
		for (int i : getMyNumbers()) {
			System.out.print(" " + i);
		}
		System.out.println();
	}

	public static void main(String[] args) {

		Lotto myLotto = new Lotto();
		myLotto.drawNumbers();
		myLotto.printNumbers();
		
	}

}
