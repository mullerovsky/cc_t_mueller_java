package homework.arrays;

import java.lang.Math;

public class Array01_02_03 {
	// private long[] myArray;

	static private long[] createArray(int length) {
		long[] a = new long[length];
		return a;
	}

	static public long[] fillArray(int length) {
		long[] a = createArray(length);
		for (int i = 0; i < a.length; i++) {
			a[i] = i + 1;
		}
		return a;
	}

	static public long[] reverseFillArray(int length) {
		long[] a = createArray(length);
		for (int i = 0; i < a.length; i++) {
			a[i] = length - i;
		}
		return a;
	}

	static public long[] randomFillArray(int length) {

		long[] a = createArray(length);
		for (int i = 0; i < a.length; i++) {
			a[i] = (int) Math.ceil(length * Math.random());
		}

		return a;
	}

	static public void printArray(long[] a) {
		System.out.print("Array: ");
		for (long i : a) {
			System.out.print(" " + i);
		}
		System.out.println();
	}

	static public long sum(long[] a) {
		long sum = 0;
		for (long i : a) {
			sum += i;
		}
		return sum;
	}

	static public double mean(long[] a) {
		return (double)sum(a) / a.length;
	}

	static public long min(long[] a) {
		long min = a[0];
		for (int i = 1; i < a.length; i++) {
			if (a[i] < min) {
				min = a[i];
			}
		}
		return min;
	}

	static public long max(long[] a) {
		long max = a[0];
		for (int i = 1; i < a.length; i++) {
			if (a[i] > max) {
				max = a[i];
			}
		}
		return max;
	}

	static public void reverse(long[] a) {
		for (int i = 0; i < a.length/2; i++) {
			long tmp = a[i];
			a[i] = a[a.length-i-1];
			a[a.length-i-1] = tmp;			
		}
	}
	
	public static void main(String[] args) {
		long[] a;

		System.out.println("Array 1 - 10");
		a = fillArray(10);
		printArray(a);
		System.out.println();

		System.out.println("Array 10 - 1");
		a = reverseFillArray(10);
		printArray(a);
		System.out.println();

		System.out.println("Array random");
		a = randomFillArray(10);
		printArray(a);
		System.out.println();

		System.out.println("Summe:\t\t" + sum(a));
		System.out.println("Mittelwert:\t" + mean(a));
		System.out.println("Minimum:\t" + min(a));
		System.out.println("Maximum:\t" + max(a));

		System.out.println("Array random");
		a = randomFillArray(10);
		printArray(a);
		System.out.println();
		
		System.out.println("Array reversed");
		reverse(a);
		printArray(a);
		System.out.println();
		
		
	}

}
