package homework.arrays;

public class FrequencyTest {
	private byte[] randNums;
	private int[] frequency = new int[10];

	public byte[] getRandNums() {
		return this.randNums;
	}

	public void setRandNums(int length) {
		this.randNums = new byte[length];
		for (int i = 0; i < length; i++) {
			this.randNums[i] = (byte) Math.floor(10 * Math.random());
		}
	}

	public int[] getFrequency() {
		return this.frequency;
	}

	public void setFrequency() {
		this.frequency = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		for (int i = 0; i < randNums.length; i++) {
			this.frequency[randNums[i]]++;
		}
	}

	public void printFrequency() {
		System.out.printf("Häufigkeiten für %d Zufallszahlen von 0 bis 9 \n", getRandNums().length);
		for (int i = 0; i < 10; i++) {
			System.out.printf("Häufigkeit von %d ist %d.\n", i, getFrequency()[i]);
		}
		System.out.println();
	}

	public FrequencyTest() {
	}

	public FrequencyTest(int length) {
		setRandNums(length);
		setFrequency();
	}

	public static void main(String[] args) {
		FrequencyTest myFrequencyTest = new FrequencyTest(50000000);
		myFrequencyTest.printFrequency();
	}

}
